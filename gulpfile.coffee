gulp = require 'gulp'
concat = require 'gulp-concat'
pug = require 'gulp-pug'

gulp.task 'clean', ->
    require('del') ['dist', 'bin']

gulp.task 'copy:scripts', ->
    gulp.src 'src/scripts/*.js'
        .pipe gulp.dest 'dist/scripts'

gulp.task 'build:content:js', ->
    gulp.src([
            'node_modules/jquery/dist/jquery.min.js'
            'src/content/utils.js'
            'src/content/boards-nav-icon.js'
            'src/content/project-picker-links.js'
        ])
        .pipe concat 'content.js'
        .pipe gulp.dest 'dist/scripts'

gulp.task 'build:options:js', ->
    gulp.src([
            'src/shared/storage.js'
            'src/options/*.js'
        ])
        .pipe concat 'options.js'
        .pipe gulp.dest 'dist/scripts'

gulp.task 'build:content:css', ->
    gulp.src([
            'src/content/*.css'
        ])
        .pipe concat 'content.css'
        .pipe gulp.dest 'dist/styles'

gulp.task 'build:options:pages', ->
    gulp.src('src/options/*.pug')
        .pipe pug()
        .pipe gulp.dest 'dist/options'

gulp.task 'build:background', ->
    gulp.src([
            'node_modules/fuse.js/src/fuse.min.js'
            'src/shared/storage.js'
            'src/background/omnibox.js'
            'src/background/background.js'
        ])
        .pipe concat 'background.js'
        .pipe gulp.dest 'dist/scripts'

gulp.task 'copy:manifest', ->
    gulp.src 'src/manifest.json'
        .pipe gulp.dest 'dist'

gulp.task 'copy:images', ->
    gulp.src 'src/images/*.*'
        .pipe gulp.dest 'dist/images'

gulp.task 'build', [
    'build:content:js'
    'build:content:css'
    'build:options:js'
    'build:options:pages'
    'build:background'
    'copy:scripts'
    'copy:manifest'
    'copy:images'
]

gulp.task 'watch', ->
    gulp.watch 'src/shared/*.js',     ['build:background', 'build:options:js']
    gulp.watch 'src/background/*.js', ['build:background']
    gulp.watch 'src/content/*.js',    ['build:content:js']
    gulp.watch 'src/content/*.css',   ['build:content:css']
    gulp.watch 'src/options/*.js',    ['build:options:js']
    gulp.watch 'src/options/*.pug',   ['build:options:pages']
    gulp.watch 'src/scripts/*.js',    ['copy:scripts']
    gulp.watch 'src/manifest.json',   ['copy:manifest']

gulp.task 'pack', ['build'], () ->
    {'7z' : _7z} = require('7zip')
    {spawn} = require('child_process')
    spawn(_7z, ['a', 'bin\\extension.zip', '.\\dist\\*'])