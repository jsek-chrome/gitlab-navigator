class Storage {
  get(propertyKey) {
    return new Promise((resolve, reject) => {
      chrome.storage.local.get(propertyKey, o => resolve(o[propertyKey]));
    })
  }

  set(propertyKey, value) {
    let o = {};
    o[propertyKey] = value;
    chrome.storage.local.set(o);
  }

  ////////////////////////////////////////
  // Configuration
  //

  setOmniboxTargetSuffix(suffix) {
    this.set('OMNIBOX_SUFFIX', suffix);
  }
  getOmniboxTargetSuffix() {
    return this.get('OMNIBOX_SUFFIX');
  }

  setProjectPickerTargetSuffix(suffix) {
    this.set('PICKER_SUFFIX', suffix);
  }
  getProjectPickerTargetSuffix() {
    return this.get('PICKER_SUFFIX');
  }

  ////////////////////////////////////////
  // Cache
  //

  saveProjects(projectsData) {
    this.set('PROJECTS', {
      projects: projectsData,
      timestamp: new Date()
    });
  }

  getProjects() {
    return this.get('PROJECTS')
      .then(data => Promise.resolve(data ? data.projects : null));
  }
}

const storage = new Storage();