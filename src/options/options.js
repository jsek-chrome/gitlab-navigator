function ready(fn) {
    if (document.readyState != 'loading') {
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}

_ = (selector) => document.getElementById(selector)

ready(() => {
    storage.getOmniboxTargetSuffix()
        .then(suffix => _('omnibox-target-suffix').value = suffix)

    storage.getProjectPickerTargetSuffix()
        .then(suffix => _('picker-target-suffix').value = suffix)
})

_('save').addEventListener('click', () => {
    storage.setOmniboxTargetSuffix(_('omnibox-target-suffix').value);
    storage.setProjectPickerTargetSuffix(_('picker-target-suffix').value);
    chrome.runtime.sendMessage({ type: 'broadcast-options-changed' }, close);
})