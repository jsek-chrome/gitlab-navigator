//////////////////////////////////
// Toggle project picker

(() => {
    let event = document.createEvent('HTMLEvents');
    event.initEvent('click', true, false);
    document.getElementsByClassName('js-projects-dropdown-toggle')[0].dispatchEvent(event);
})();