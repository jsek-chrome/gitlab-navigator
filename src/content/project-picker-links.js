let suffix = '';
let observer = null;
const lastPathPartPattern = /\/[^\/]+$/;
const projectUrlPattern = /https:\/\/gitlab.com\/[^\/]+\/[^\/]+/;
const projectPickerLinks = '.js-dropdown-menu-projects .dropdown-content a';

let appendSuffix = (linkElement) => {
    linkElement.href = linkElement.href.match(projectUrlPattern)[0] + suffix;
}

let hasSuffix = (linkElement) => {
    linkElement.href.match(lastPathPartPattern)[0] === suffix;
}

let ensurePickerLinksHaveSuffix = (mutations) => {
    let elements = [].slice.call(document.querySelectorAll(projectPickerLinks));
    if (elements.length) {
        if (!hasSuffix(elements[0])) {
            elements.forEach(appendSuffix);
        }
    }
};

let getSuffix = () =>
    new Promise(resolve =>
        chrome.runtime.sendMessage({
            type: 'get-picker-suffix'
        }, resolve)
    );

let onOptionsChanged = (fn) =>
    chrome.runtime.onMessage.addListener((message, sender, sendResponse) =>
        (message.type === 'options-changed') ? fn() : true
    );

let resetObserver = (newSuffix) => {
    suffix = newSuffix;
    ensurePickerLinksHaveSuffix();
    if (suffix.length > 1 && observer === null) {
        observer = Utils.SetRootMutationObserver(ensurePickerLinksHaveSuffix);
    } else if (observer) {
        observer.disconnect();
        observer = null;
    }
}

let init = () => {
    getSuffix().then(resetObserver);
}

$(() => {
    init();
    onOptionsChanged(init);
});