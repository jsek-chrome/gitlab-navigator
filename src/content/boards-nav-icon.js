const projectPathPattern = /^\/[^\/]+\/[^\/]+/;
const iconsContainer = '.header-content .header-user';

let isProjectContext = () => projectPathPattern.test(location.pathname) && $('.js-projects-dropdown-toggle').length

let attachIcon = () => {
    if (!isProjectContext()) return;

    let projectPath = location.pathname.match(projectPathPattern)[0];
    $(iconsContainer).prev().before(`
            <li id="ext-issue-boards-link">
                <a title="Board" aria-label="Board" data-toggle="tooltip" href="${projectPath}/boards">
                    <i class="fa fa-columns fa-fw"></i>
                </a>
            </li>
        `);
}

let ensureIconExists = (mutations) => {
    if (!document.getElementById('ext-issue-boards-link')) {
        attachIcon();
    }
};

$(() => {
    setTimeout(attachIcon, 10);
    Utils.SetRootMutationObserver(ensureIconExists);
});