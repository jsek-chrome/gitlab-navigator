class Utils {
    static _debounce(callback, wait, context = this) {
        // from https://gist.github.com/beaucharman/1f93fdd7c72860736643d1ab274fee1a
        let timeout = null
        let callbackArgs = null

        const later = () => callback.apply(context, callbackArgs)

        return function () {
            callbackArgs = arguments
            clearTimeout(timeout)
            timeout = setTimeout(later, wait)
        }
    }

    static SetRootMutationObserver(fn) {
        let m = new MutationObserver(Utils._debounce(fn, 100));
        m.observe($('html')[0], {
            childList: true,
            subtree: true
        });
        return m;
    }
}