class OmniboxController {

    constructor() {
        this.projectsCache = undefined;
    }

    onInputStarted() {
        // docs: https://docs.gitlab.com/ee/api/README.html
        fetch('https://gitlab.com/api/v3/projects?simple=true&per_page=100', {
                credentials: 'include'
            })
            .then(response => response.json().then(this.updateProjectsCache.bind(this)))
            .catch(err => {
                this.cacheUpdateProblem = err;
                console.warn('Failed to fetch projects', err);
            });

        storage.getOmniboxTargetSuffix()
            .then(suffix => this.urlSuffix = suffix || '');
    }

    onInputChanged(text, callback) {
        if (!this.projectsCache) {
            storage.getProjects()
                .then(projects => {
                    this.projectsCache = projects;
                    this.generateSuggestions(text)
                        .then(callback);
                })
        } else {
            this.generateSuggestions(text)
                .then(callback);
        }
    }

    updateProjectsCache(projectsApiResponse) {

        let relevantProjectsData = projectsApiResponse.map(p => {
            return {
                url: p.web_url,
                name: p.name_with_namespace,
                path: p.path_with_namespace
            }
        })

        this.projectsCache = relevantProjectsData;

        storage.saveProjects(relevantProjectsData);
    }

    generateSuggestions(text) {
        return new Promise((resolve, reject) => {

            if (!this.projectsCache) {
                resolve([{
                    content: 'https://gitlab.com/users/sign_in',
                    description: 'Sign in to GitLab',
                }]);
            }

            let matchedProjects = new Fuse(this.projectsCache, {
                keys: ['name', 'path']
            }).search(text);

            let suggestions = matchedProjects
                .map(p => {
                    return {
                        content: p.url + this.urlSuffix,
                        description: p.name
                    }
                });

            resolve(suggestions);
        });
    }

    navigateToMatchedProject(text) {
        if (text.indexOf('https://gitlab.com/') === 0) {
            this.open(text);
        }
        this.generateSuggestions(text)
            .then(suggestions => {
                if (suggestions.length > 0) {
                    this.open(suggestions[0].content);
                }
            })
    }

    open(url) {
        chrome.tabs.getSelected(({
            id,
            url: currentUrl
        }) => {
            if (/^http/.test(currentUrl)) {
                chrome.tabs.executeScript(null, {
                    code: `location.href = '${url}'`
                });
            } else {
                chrome.tabs.update(id, {
                    url
                });
            }
        });
    }
}


const controller = new OmniboxController();

chrome.omnibox.onInputStarted.addListener(() => {
    controller.onInputStarted();
});

chrome.omnibox.onInputChanged.addListener((text, suggest) => {
    controller.onInputChanged(text, suggest);
});

chrome.omnibox.onInputEntered.addListener((text) => {
    controller.navigateToMatchedProject(text);
});