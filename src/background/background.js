chrome.commands.onCommand.addListener(function (command) {
  switch (command) {
    case 'switch-project': {
      chrome.tabs.executeScript(null, { file: 'scripts/switch-project.js' });
      break;
    }
    default: {
      console.warn('Unrecognized command');
    }
  }
});

chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
  switch (message.type) {
    case 'save-favorite-project': {
      let { url, title } = message.data;
      storage.saveFavoriteProject({ url, title });
      sendResponse({isSuccess: true});
      break;
    }
    case 'get-picker-suffix': {
      storage.getProjectPickerTargetSuffix()
        .then(sendResponse);
      break;
    }
    case 'broadcast-options-changed':
      chrome.tabs.query({}, tabs => {
        tabs.forEach(tab =>
          chrome.tabs.sendMessage(tab.id, { type: 'options-changed' })
        );
        sendResponse();
      });
      break;
    default: {
      console.warn(`Unrecognized message type [${message.type}]`);
    }
  }
  return true;
});

