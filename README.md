# GitLab Navigator

Navigation utility that adds handy quick links and allows quickly switch between projects in GitLab.

---

## Installation

You can find it on [Chrome Web Store](https://chrome.google.com/webstore/detail/gitlab-navigator/kflaahoabcdldidfobnldaafbgegilel)

## Functionality

- [x] Board shortcut link when current page is in project context
- Jump navigation between projects
  - [x] Toggle project picker
  - [x] Better position of projects picker
  - [x] Options: Custom target for navigation from project picker
  - [ ] Ability to mark favorite projects
- [x] Address bar autocomplete
  - [x] Options: Custom target for navigation from address bar
- [ ] Shortcut (and status indicator) to currently running pipeline
- [ ] History of last pipelines across all projects

---

## Setup

```bash
npm install
```

### Build

```bash
npm run build
```

### Pack before publish

```bash
npm run pack
```

- Zip archive is used to upload new version to Chrome Store.

## Credits

Icons made by [Freepik](http://www.freepik.com) from [Flaticon](http://www.flaticon.com) is licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/)